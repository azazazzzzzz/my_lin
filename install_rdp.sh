#!/bin/bash
dd if=/dev/zero of=/var/swapd bs=1024 count=3145728
mkswap /var/swapd
chmod 0644 /var/swapd
swapon /var/swapd
echo '/var/swapd   swap   swap   default 0 0' >> /etc/fstab

dd if=/dev/zero of=/var/swapd1 bs=1024 count=3145728
mkswap /var/swapd1
chmod 0644 /var/swapd1
swapon /var/swapd1
echo '/var/swapd1   swap   swap   default 0 0' >> /etc/fstab

dd if=/dev/zero of=/var/swapd2 bs=1024 count=3145728
mkswap /var/swapd2
chmod 0644 /var/swapd2
swapon /var/swapd2
echo '/var/swapd2   swap   swap   default 0 0' >> /etc/fstab

dd if=/dev/zero of=/var/swapd3 bs=1024 count=3145728
mkswap /var/swapd3
chmod 0644 /var/swapd3
swapon /var/swapd3
echo '/var/swapd3   swap   swap   default 0 0' >> /etc/fstab

apt update && apt -y upgrade
 
apt-get install -y gnupg2 curl unzip apt-transport-https gnupg2
 
export codename="$(find /etc/apt -type f -name '*.list' | xargs grep -E '^deb' | awk '{print $3}' | grep -Eo '^[a-z]+' | sort | uniq -c | sort -n | tail -n1 | grep -Eo '[a-z]+$')"
 
echo -e "deb http://liquorix.net/debian $codename main\ndeb-src http://liquorix.net/debian $codename main\n\n# Mirrors:\n#\n# Unit193 - France\n# deb http://mirror.unit193.net/liquorix $codename main\n# deb-src http://mirror.unit193.net/liquorix $codename main" | tee /etc/apt/sources.list.d/liquorix.list
 
wget https://liquorix.net/linux-liquorix.pub
apt-key add linux-liquorix.pub
apt-get update
 
apt-get -y install linux-image-liquorix-amd64 linux-headers-liquorix-amd64

#cd
#apt-get update -y
#apt-get dist-upgrade -y

apt-get install sudo curl wget patch -y
apt --fix-broken install -y
 
apt-get install apt-transport-https -y

apt-get install x-window-system-core -y

#apt-get install lxde lubuntu-core lubuntu-icon-theme lubuntu-restricted-extras locales vim -y
#apt-get install --no-install-recommends lubuntu-desktop

#apt-get install task-xfce-desktop locales vim -y
apt-get install xfce4 locales vim -y

#apt-get install mate-desktop-environment-extras locales vim -y
 
apt-get install xfonts-intl-chinese xfonts-wqy -y

apt-get install fontforge software-properties-common -y

apt-get install ibus-libpinyin net-tools network-manager network-manager-gnome -y

echo '[main]
plugins=ifupdown,keyfile
 
[ifupdown]
managed=true'>/etc/NetworkManager/NetworkManager.conf
 
apt-get install xrdp tigervnc-standalone-server -y
#apt-get install tightvncserver xrdp -y

apt-get install -y gcc autoconf libtool automake make zlib1g-dev openssl asciidoc xmlto git net-tools curl lrzsz libjemalloc-dev
  
apt-get install -y libbz2-dev libreadline-dev libsqlite3-dev make build-essential libssl-dev zlib1g-dev libbz2-dev libsqlite3-dev

#apt build-dep python-tk -y

apt build-dep python-tk python3-tk -y

apt-get install -y libpcre3-dev libidn2-dev libffi-dev libevent-dev

cd /opt
git clone https://github.com/pyenv/pyenv.git .pyenv

echo 'export PYENV_ROOT="/opt/.pyenv"' >> ~/.bash_profile
echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bash_profile
source ~/.bash_profile

# 主力pypy3.6
CONFIGURE_OPTS=--enable-optimizations pyenv install --verbose pypy3.6-7.3.1
echo 'export PATH="$PYENV_ROOT/versions/pypy3.6-7.3.1/bin:$PATH"' >> ~/.bash_profile
source ~/.bash_profile

# 备用3.6.9
CONFIGURE_OPTS=--enable-optimizations pyenv install --verbose 3.6.9

#time CONFIGURE_OPTS=--enable-optimizations pyenv install --verbose 3.5.9
#pyenv install --verbose 3.5.9

#echo 'export PATH="$PYENV_ROOT/versions/3.5.9/bin:$PATH"' >> ~/.bash_profile
#source ~/.bash_profile

#echo 'export PATH="$PYENV_ROOT/versions/3.7.3/bin:$PATH"' >> ~/.bash_profile
#source ~/.bash_profile

#time CONFIGURE_OPTS=--enable-optimizations pyenv install --verbose 3.7.7
#echo 'export PATH="$PYENV_ROOT/versions/3.7.7/bin:$PATH"' >> ~/.bash_profile
#source ~/.bash_profile

# 编辑ini的部分怎么处理
nano /etc/xrdp/xrdp.ini

# 修改为 max_bpp=24
# 把xvnc拉到最顶部
[Xvnc]
name=Xvnc
lib=libvnc.so
username=ask
password=ask
ip=127.0.0.1
port=-1
#xserverbpp=24
#delay_ms=2000


cd
wget https://gitlab.com/azazazzzzzz/my_lin/-/raw/master/iron64.deb
apt -y install ./iron64.deb

rm iron64.deb


adduser john
 
touch ~/.Xclients
#echo "mate-session" > ~/.Xclients
echo "xfce4-session" > ~/.Xclients
#echo "lxsession -s LXDE -e LXDE" > ~/.Xclients
chmod a+x ~/.Xclients
touch /home/john/.Xclients
#echo "mate-session" > /home/john/.Xclients
echo "xfce4-session" > /home/john/.Xclients
#echo "lxsession -s LXDE -e LXDE" > /home/john/.Xclients
chmod a+x /home/john/.Xclients
chown john:john /home/john/.Xclients
systemctl restart xrdp
 
su - john
#echo 'export PYENV_ROOT="/opt/.pyenv"' >> ~/.bash_profile
#echo 'export PATH="$PYENV_ROOT/versions/3.5.7/bin:$PATH"' >> ~/.bash_profile

#echo 'export PYENV_ROOT="/opt/.pyenv"' >> ~/.bash_profile
#echo 'export PATH="$PYENV_ROOT/versions/3.7.3/bin:$PATH"' >> ~/.bash_profile

echo 'export PYENV_ROOT="/opt/.pyenv"' >> ~/.bashrc
echo 'export PATH="$PYENV_ROOT/versions/pypy3.6-7.3.1/bin:$PATH"' >> ~/.bashrc

exit

cp /etc/security/limits.conf ~/limits.conf.bak
cat > /etc/security/limits.conf <<-EOF
* soft nofile 51200
* hard nofile 51200
EOF
cp /etc/profile ~/profile.bak
cat >> /etc/profile <<-EOF
ulimit -SHn 51200
EOF
cp /etc/sysctl.conf ~/sysctl.conf.bak
cat > /etc/sysctl.conf <<-EOF
fs.file-max = 51200
net.core.rmem_max = 67108864
net.core.wmem_max = 67108864
net.core.rmem_default = 65536
net.core.wmem_default = 65536
net.core.netdev_max_backlog = 4096
net.core.somaxconn = 4096
net.ipv4.tcp_syncookies = 1
net.ipv4.tcp_tw_reuse = 1
net.ipv4.tcp_fin_timeout = 30
net.ipv4.tcp_keepalive_time = 1200
net.ipv4.ip_local_port_range = 10000 65000
net.ipv4.tcp_max_syn_backlog = 4096
net.ipv4.tcp_max_tw_buckets = 5000
net.ipv4.tcp_fastopen = 3
net.ipv4.tcp_rmem = 4096 87380 67108864
net.ipv4.tcp_wmem = 4096 65536 67108864
net.ipv4.tcp_mtu_probing = 1
net.ipv4.ip_forward=1
#net.ipv6.conf.all.disable_ipv6 = 1
net.core.default_qdisc = fq
net.ipv4.tcp_congestion_control = bbr
EOF
sysctl -p

reboot

# ubuntu
apt-get install xubuntu-desktop pluma sakura -y
# debian
apt-get install xfce4-goodies pluma sakura -y