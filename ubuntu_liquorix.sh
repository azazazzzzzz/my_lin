apt update
apt -y install software-properties-common net-tools
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 9352A0B69B72E6DF
cat > /etc/apt/sources.list.d/damentz-ubuntu-liquorix-focal.list <<-EOF
deb http://ppa.launchpad.net/damentz/liquorix/ubuntu focal main
# deb-src http://ppa.launchpad.net/damentz/liquorix/ubuntu focal main
EOF
apt update
apt-get -y install linux-image-liquorix-amd64 linux-headers-liquorix-amd64
cp /etc/security/limits.conf ~/limits.conf.bak
cat > /etc/security/limits.conf <<-EOF
* soft nofile 51200
* hard nofile 51200
EOF
cp /etc/profile ~/profile.bak
cat >> /etc/profile <<-EOF
ulimit -SHn 51200
EOF
cp /etc/sysctl.conf ~/sysctl.conf.bak
cat > /etc/sysctl.conf <<-EOF
fs.file-max = 51200
net.core.rmem_max = 67108864
net.core.wmem_max = 67108864
net.core.rmem_default = 65536
net.core.wmem_default = 65536
net.core.netdev_max_backlog = 4096
net.core.somaxconn = 4096
net.ipv4.tcp_syncookies = 1
net.ipv4.tcp_tw_reuse = 1
net.ipv4.tcp_fin_timeout = 30
net.ipv4.tcp_keepalive_time = 1200
net.ipv4.ip_local_port_range = 10000 65000
net.ipv4.tcp_max_syn_backlog = 4096
net.ipv4.tcp_max_tw_buckets = 5000
net.ipv4.tcp_fastopen = 3
net.ipv4.tcp_rmem = 4096 87380 67108864
net.ipv4.tcp_wmem = 4096 65536 67108864
net.ipv4.tcp_mtu_probing = 1
net.ipv4.ip_forward=1
#net.ipv6.conf.all.disable_ipv6 = 1
net.core.default_qdisc = fq
net.ipv4.tcp_congestion_control = bbr
EOF
sysctl -p
wget https://install.direct/go.sh && bash go.sh
reboot
